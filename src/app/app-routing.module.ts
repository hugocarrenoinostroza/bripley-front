import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticatedGuard } from './bripley-authentication/guards/authenticated.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'products'
  },
  {
    path: 'login',
    loadChildren: () => import('./bripley-login/bripley-login.module').then(m => m.BripleyLoginModule),
  },
  {
    path: 'products',
    canActivateChild: [AuthenticatedGuard],
    loadChildren: () => import('./bripley-products/bripley-products.module').then(m => m.BripleyProductsModule)
  },
  {
    path: 'products/:id',
    canActivateChild: [AuthenticatedGuard],
    loadChildren: () => import('./bripley-product/bripley-product.module').then(m => m.BripleyProductModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
