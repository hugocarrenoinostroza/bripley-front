import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Credentials } from '../../bripley-authentication/models/Credentials';

@Component({
  selector: 'bripley-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Input()
  loggingIn: boolean;

  @Input()
  authError: string;

  @Output()
  login: EventEmitter<Credentials> = new EventEmitter();

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.createLoginForm();
  }

  createLoginForm(): FormGroup {
    return this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  emitLogin() {
    if (this.loginForm.valid) {
      this.login.emit(this.loginForm.value);
    }
  }

}
