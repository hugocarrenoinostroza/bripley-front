import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { LoginFormComponent } from './login-form.component';


describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginFormComponent],
      imports: [ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create login form', () => {
    expect(component.loginForm).toBeTruthy();
  });

  it('loginForm should have username, password', () => {
    expect(component.loginForm.get('username')).toBeTruthy();
    expect(component.loginForm.get('password')).toBeTruthy();
  });

  it('username, password should be required', () => {
    expect(component.loginForm.get('username').hasError('required')).toBeTruthy();
    expect(component.loginForm.get('password').hasError('required')).toBeTruthy();
  });

  it('username should be email', () => {
    component.loginForm.get('username').setValue('username');
    expect(component.loginForm.get('username').hasError('email')).toBeTruthy();
  });

  it('given a valid loginForm  should emit event', () => {

    component.loginForm.get('username').setValue('email@email.com');
    component.loginForm.get('password').setValue('test');

    const loginOutput = spyOn(component.login, 'emit');

    component.emitLogin();

    expect(loginOutput).toHaveBeenCalled();

  });

  it('click on submit should call emitLogin', () => {

    component.loginForm.get('username').setValue('email@email.com');
    component.loginForm.get('password').setValue('test');

    const emitLogin = spyOn(component, 'emitLogin');

    const loginButton: DebugElement = fixture.debugElement.query(By.css('.login__submit'));

    loginButton.triggerEventHandler('click', {});

    expect(emitLogin).toHaveBeenCalled();

  });


});
