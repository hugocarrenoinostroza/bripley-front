import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Credentials } from '../../bripley-authentication/models/Credentials';
import { AuthenticationFacade, AUTHENTICATION_FACADE } from '../../bripley-authentication/services/authentication-facade/authentication.facade';

@Component({
  selector: 'bripley-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loggingIn: Observable<boolean>;
  authError: Observable<string>;

  constructor(@Inject(AUTHENTICATION_FACADE) private authenticationFacade: AuthenticationFacade) { }

  ngOnInit(): void {
    this.loggingIn = this.authenticationFacade.authenticating();
    this.authError = this.authenticationFacade.getAuthenticationError();
  }

  login(credential: Credentials) {
    this.authenticationFacade.authenticate(credential);
  }

}
