import { Component, EventEmitter, Input, NO_ERRORS_SCHEMA, Output } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BehaviorSubject, Subject } from 'rxjs';
import { Credentials } from 'src/app/bripley-authentication/models/Credentials';
import { AuthenticationFacade, AUTHENTICATION_FACADE } from 'src/app/bripley-authentication/services/authentication-facade/authentication.facade';
import { LoginComponent } from './login.component';

@Component({
  selector: 'bripley-login-form',
  template: 'mock login form'
})
export class LoginFormMockComponent {

  @Input()
  loggingIn: boolean;

  @Input()
  authError: string;

  @Output()
  login: EventEmitter<Credentials> = new EventEmitter();

}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let authenticationFacadeSpy: jasmine.SpyObj<AuthenticationFacade>;

  let authenticating: Subject<boolean>;
  let authError: Subject<string>;

  beforeEach(async(() => {

    const spy = jasmine.createSpyObj('AuthenticationFacade', ['authenticating', 'getAuthenticationError', 'authenticate']);

    TestBed.configureTestingModule({
      declarations: [LoginComponent, LoginFormMockComponent],
      providers: [{
        provide: AUTHENTICATION_FACADE,
        useValue: spy
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();

    authenticationFacadeSpy = TestBed.inject(AUTHENTICATION_FACADE) as jasmine.SpyObj<AuthenticationFacade>;

    authenticating = new BehaviorSubject<boolean>(false);
    authenticationFacadeSpy.authenticating.and.returnValue(authenticating);

    authError = new BehaviorSubject<string>('');
    authenticationFacadeSpy.getAuthenticationError.and.returnValue(authError);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assign authenticating and getAuthenticationError', () => {

    expect(authenticationFacadeSpy.authenticating).toHaveBeenCalled();
    expect(authenticationFacadeSpy.getAuthenticationError).toHaveBeenCalled();

  });

  it('when login should call authenticate', () => {
    const credentials: Credentials = { username: 'username@email.com', password: 'test' };

    component.login(credentials);

    expect(authenticationFacadeSpy.authenticate).toHaveBeenCalledWith(credentials);
  });

  it('loginFormComponent.loggingIn should be true when authentication', () => {

    authenticating.next(true);

    const loginFormComponent: LoginFormMockComponent = fixture.debugElement.query(By.directive(LoginFormMockComponent)).context;

    fixture.detectChanges();

    expect(loginFormComponent.loggingIn).toEqual(true);

  });

  it('given an authentication error loginFormComponent.authError should have the error', () => {

    const error = 'invalid password';

    authError.next(error);

    const loginFormComponent: LoginFormMockComponent = fixture.debugElement.query(By.directive(LoginFormMockComponent)).context;

    fixture.detectChanges();

    expect(loginFormComponent.authError).toEqual(error);

  });

  it('should call login when loginFormComponent emit login', () => {

    const spyLogin = spyOn(component, 'login');

    const credentials: Credentials = { username: 'username@email.com', password: 'test' };

    const loginFormComponent: LoginFormMockComponent = fixture.debugElement.query(By.directive(LoginFormMockComponent)).context;

    loginFormComponent.login.emit(credentials);

    expect(spyLogin).toHaveBeenCalledWith(credentials);

  });

});
