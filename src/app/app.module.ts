import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticatedGuard } from './bripley-authentication/guards/authenticated.guard';
import { TokenInterceptor } from './bripley-authentication/interceptors/token.interceptor';
import { AuthenticationImplFacade } from './bripley-authentication/services/authentication-facade/authentication-impl.facade';
import { AUTHENTICATION_FACADE } from './bripley-authentication/services/authentication-facade/authentication.facade';
import { AuthenticationFirebaseService } from './bripley-authentication/services/authentication-service/authentication-firebase.service';
import { AUTHENTICATION_SERVICE } from './bripley-authentication/services/authentication-service/authentication.service';
import { AuthenticationEffects } from './bripley-authentication/store/authentication.effects';
import { authenticationReducer } from './bripley-authentication/store/authentication.reducer';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserAnimationsModule,
    StoreModule.forRoot([]),
    StoreModule.forFeature('authenticationReducer', authenticationReducer),
    MatProgressBarModule,
    EffectsModule.forRoot([AuthenticationEffects]),
    MatButtonModule,
    MatIconModule,
    HttpClientModule
  ],
  providers: [
    { provide: AUTHENTICATION_FACADE, useClass: AuthenticationImplFacade },
    { provide: AUTHENTICATION_SERVICE, useClass: AuthenticationFirebaseService },
    AuthenticatedGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
