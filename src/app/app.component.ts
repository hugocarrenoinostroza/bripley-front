import { Component, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationFacade, AUTHENTICATION_FACADE } from './bripley-authentication/services/authentication-facade/authentication.facade';

@Component({
  selector: 'bripley-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  checkingAuthentication: Observable<boolean>;
  isAuthenticated: Observable<boolean>;

  constructor(@Inject(AUTHENTICATION_FACADE) private authenticationFacade: AuthenticationFacade) {

    authenticationFacade.checkAuthentication();

    this.checkingAuthentication = authenticationFacade.checkingAuthentication();
    this.isAuthenticated = authenticationFacade.isAuthenticated();
  }

  logout() {

    this.authenticationFacade.logout();

  }
}
