import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BripleyProductRoutingModule } from './bripley-product-routing.module';
import { ProductDescriptionComponent } from './components/product-description/product-description.component';
import { ProductComponent } from './components/product/product.component';



@NgModule({
  declarations: [ProductComponent, ProductDescriptionComponent],
  imports: [
    CommonModule,
    BripleyProductRoutingModule,
    MatProgressBarModule
  ]
})
export class BripleyProductModule { }
