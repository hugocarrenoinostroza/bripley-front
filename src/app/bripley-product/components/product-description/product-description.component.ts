import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/bripley-products-commons/model/product.model';

@Component({
  selector: 'bripley-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss']
})
export class ProductDescriptionComponent implements OnInit {

  @Input()
  product: Product;

  constructor() { }

  ngOnInit(): void {
  }

}
