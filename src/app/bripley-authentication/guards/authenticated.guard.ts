import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationFacade, AUTHENTICATION_FACADE } from '../services/authentication-facade/authentication.facade';

@Injectable()
export class AuthenticatedGuard implements CanActivateChild {

  constructor(@Inject(AUTHENTICATION_FACADE) private authenticationFacade: AuthenticationFacade) { }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.authenticationFacade.isAuthenticated();
  }


}
