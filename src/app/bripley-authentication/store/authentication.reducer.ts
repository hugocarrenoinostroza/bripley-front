
import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../models/User';
import * as AuthenticationActions from './authentication.actions';


export interface AuthenticationState {
  isAuthenticated: boolean;
  authenticating: boolean;
  checkingAuthentication: boolean;
  user: User;
  authError: string;
}

const initialState: AuthenticationState = {
  isAuthenticated: false,
  authenticating: false,
  checkingAuthentication: false,
  user: null,
  authError: null,
};

const onAuthentication = (state: AuthenticationState, { credentials }): AuthenticationState => {
  return {
    ...state,
    authenticating: true,
    authError: ''
  };
};

const onAuthenticationSuccess = (state: AuthenticationState, { user }): AuthenticationState => {
  return {
    ...state,
    authenticating: false,
    checkingAuthentication: false,
    isAuthenticated: true,
    user,
    authError: '',
  };
};

const onAuthenticationFailed = (state: AuthenticationState, { message }): AuthenticationState => {

  return {
    ...state,
    authenticating: false,
    isAuthenticated: false,
    checkingAuthentication: false,
    user: null,
    authError: message
  };
};

const onCheckAuthentication = (state: AuthenticationState): AuthenticationState => {
  return {
    ...state,
    checkingAuthentication: true,

  };
};

const onLogoutSuccess = (state: AuthenticationState): AuthenticationState => {
  return {
    ...state,
    isAuthenticated: false,
    user: null,
    authError: ''
  };
};

export const reducer = createReducer(initialState,
  on(AuthenticationActions.authenticate, onAuthentication),
  on(AuthenticationActions.authenticationSuccess, onAuthenticationSuccess),
  on(AuthenticationActions.authenticationFailed, onAuthenticationFailed),
  on(AuthenticationActions.checkAuthentication, onCheckAuthentication),
  on(AuthenticationActions.logoutSuccess, onLogoutSuccess),
  on(AuthenticationActions.authenticationFailed, onAuthenticationFailed),
  on(AuthenticationActions.checkAuthenticationSuccess, onAuthenticationSuccess),
);


export function authenticationReducer(state: AuthenticationState, action: Action) {
  return reducer(state, action);
}


