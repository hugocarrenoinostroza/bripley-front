import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AuthenticationService, AUTHENTICATION_SERVICE } from '../services/authentication-service/authentication.service';
import * as AuthenticationActions from './authentication.actions';

@Injectable()
export class AuthenticationEffects {

  authenticate = createEffect(() => this.actions.pipe(
    ofType(AuthenticationActions.authenticate),
    switchMap(action => this.authenticationService.authenticate(action.credentials).pipe(
      map(user => AuthenticationActions.authenticationSuccess({ user })),
      catchError(error => of(AuthenticationActions.authenticationFailed({ message: error.message })))
    )),
  )
  );

  redirectToHome = createEffect(() => this.actions.pipe(
    ofType(AuthenticationActions.authenticationSuccess),
    switchMap(() => this.router.navigate(['/products'])
    )
  ), { dispatch: false });

  checkAuthentication = createEffect(() => this.actions.pipe(
    ofType(AuthenticationActions.checkAuthentication),
    switchMap(() => this.authenticationService.checkAuthentication().pipe(
      map(user => AuthenticationActions.checkAuthenticationSuccess({ user }))
    )),
    catchError(error => of(AuthenticationActions.authenticationFailed({ message: error.message })))
  )
  );

  logout = createEffect(() => this.actions.pipe(
    ofType(AuthenticationActions.logout),
    switchMap(() => this.authenticationService.logout().pipe(
      map(() => AuthenticationActions.logoutSuccess()),
      catchError(error => of(AuthenticationActions.logoutError({ message: error })))
    )),

  ));

  redirectToLogin = createEffect(() => this.actions.pipe(
    ofType(AuthenticationActions.logoutSuccess, AuthenticationActions.authenticationFailed),
    switchMap(() => this.router.navigate(['/login']))
  ), { dispatch: false });


  constructor(
    private actions: Actions,
    @Inject(AUTHENTICATION_SERVICE) private authenticationService: AuthenticationService,
    private router: Router
  ) { }



}
