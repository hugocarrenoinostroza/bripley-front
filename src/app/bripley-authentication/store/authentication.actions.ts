import { createAction, props } from '@ngrx/store';
import { Credentials } from '../models/Credentials';
import { User } from '../models/User';

export const authenticate = createAction('[Authentication] Authenticate', props<{ credentials: Credentials }>());
export const authenticationSuccess = createAction('[Authentication] Authentication Success', props<{ user: User }>());
export const authenticationFailed = createAction('[Authentication] Authentication Failed', props<{ message: string }>());
export const checkAuthentication = createAction('[Authentication] Check Authentication');
export const checkAuthenticationSuccess = createAction('[Authentication] Check Authentication Success', props<{ user: User }>());
export const logout = createAction('[Authentication] Logout');
export const logoutSuccess = createAction('[Authentication] Logout Success');
export const logoutError = createAction('[Authentication] Logout Error', props<{ message: string }>());
