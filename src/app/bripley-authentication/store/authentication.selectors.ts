import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthenticationState } from './authentication.reducer';

export const authenticationState = createFeatureSelector<AuthenticationState>('authenticationReducer');
export const authenticating = createSelector(authenticationState, state => state.authenticating);
export const isAuthenticated = createSelector(authenticationState, state => state.isAuthenticated);
export const checkingAuthentication = createSelector(authenticationState, state => state.checkingAuthentication);
export const user = createSelector(authenticationState, state => state.user);
export const authError = createSelector(authenticationState, state => state.authError);
