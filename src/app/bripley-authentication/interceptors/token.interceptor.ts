import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { AUTHENTICATION_FACADE, AuthenticationFacade } from '../services/authentication-facade/authentication.facade';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(@Inject(AUTHENTICATION_FACADE) private authenticationFacade: AuthenticationFacade) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.authenticationFacade.getCurrentUser().pipe(
      take(1),
      switchMap(currentUser => {

        const clonedRequest = req.clone({
          headers: req.headers.set(
            'Authorization',
            `Bearer ${currentUser.token}`
          )
        });

        return next.handle(clonedRequest);

      })
    );
  }

}
