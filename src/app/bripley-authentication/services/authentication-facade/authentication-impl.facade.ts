import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map, skipWhile } from 'rxjs/operators';
import { Credentials } from '../../models/Credentials';
import { User } from '../../models/User';
import * as AuthenticationActions from '../../store/authentication.actions';
import * as AuthenticationSelectors from '../../store/authentication.selectors';
import { AuthenticationFacade } from './authentication.facade';

@Injectable()
export class AuthenticationImplFacade implements AuthenticationFacade {

  constructor(private store: Store) { }

  authenticate(credentials: Credentials) {
    this.store.dispatch(AuthenticationActions.authenticate({ credentials }));
  }

  logout() {
    this.store.dispatch(AuthenticationActions.logout());
  }

  getCurrentUser(): Observable<User> {
    return this.store.pipe(select(AuthenticationSelectors.user));
  }

  isAuthenticated(): Observable<boolean> {

    const isAuthenticatedSelector = this.store.pipe(select(AuthenticationSelectors.isAuthenticated));

    const checkingAuthenticationSelector = this.store.pipe(select(AuthenticationSelectors.checkingAuthentication));

    return combineLatest([isAuthenticatedSelector, checkingAuthenticationSelector]).pipe(
      skipWhile(([, checkingAuthentication]) => checkingAuthentication),
      map(([isAuthenticated,]) => isAuthenticated)
    );
  }

  authenticating(): Observable<boolean> {
    return this.store.pipe(select(AuthenticationSelectors.authenticating));
  }

  checkAuthentication() {
    this.store.dispatch(AuthenticationActions.checkAuthentication());
  }

  checkingAuthentication(): Observable<boolean> {
    return this.store.select(AuthenticationSelectors.checkingAuthentication);
  }

  getAuthenticationError(): Observable<any> {
    return this.store.select(AuthenticationSelectors.authError);
  }

}
