import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { Credentials } from '../../models/Credentials';
import { User } from '../../models/User';

export const AUTHENTICATION_FACADE = new InjectionToken<AuthenticationFacade>('AuthenticationFacade');

export interface AuthenticationFacade {

  authenticate(credentials: Credentials);

  logout();

  getCurrentUser(): Observable<User>;

  isAuthenticated(): Observable<boolean>;

  authenticating(): Observable<boolean>;

  checkAuthentication();

  checkingAuthentication(): Observable<boolean>;

  getAuthenticationError(): Observable<string>;


}
