import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseError } from 'firebase';
import { combineLatest, from, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Credentials } from '../../models/Credentials';
import { User } from '../../models/User';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthenticationFirebaseService implements AuthenticationService {

  constructor(private angularFireAuth: AngularFireAuth) { }

  authenticate(credentials: Credentials): Observable<User> {

    return from(this.angularFireAuth.signInWithEmailAndPassword(credentials.username, credentials.password)).pipe(
      switchMap(response => combineLatest([of(response.user.displayName), from(response.user.getIdToken())])),
      map(([displayName, idToken]) => ({ name: displayName, token: idToken })),
      catchError(error => {
        throw Error(this.mapError(error));
      })
    );

  }

  logout(): Observable<any> {
    return from(this.angularFireAuth.signOut());
  }

  checkAuthentication(): Observable<User> {
    return from(this.angularFireAuth.user).pipe(
      switchMap(user => combineLatest([of(user.displayName), from(user.getIdToken())])),
      map(([displayName, idToken]) => ({ name: displayName, token: idToken })),
      catchError(() => {
        throw Error('');
      })
    );
  }

  private mapError(error: FirebaseError) {

    const errorMap = new Map([
      ['auth/user-not-found', 'Correo no encontrado'],
      ['auth/wrong-password', 'Password incorrecto'],
      ['auth/too-many-requests', 'Demasiados intentos, por favor intenta más tarde']
    ]);

    return errorMap.get(error.code);

  }


}
