import { Credentials } from '../../models/Credentials';
import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/User';

export const AUTHENTICATION_SERVICE = new InjectionToken<AuthenticationService>('AuthenticationService');

export interface AuthenticationService {

  authenticate(credentials: Credentials): Observable<User>;
  logout(): Observable<any>;
  checkAuthentication(): Observable<User>;

}
