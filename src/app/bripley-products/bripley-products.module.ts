import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BripleyProductsRoutingModule } from './bripley-products-routing.module';
import { ProductsCatalogComponent } from './components/products-catalog/products-catalog.component';
import { ProductsHeaderComponent } from './components/products-header/products-header.component';
import { ProductsItemComponent } from './components/products-item/products-item.component';
import { ProductsComponent } from './components/products/products.component';



@NgModule({
  declarations: [ProductsComponent, ProductsCatalogComponent, ProductsItemComponent, ProductsHeaderComponent],
  imports: [
    CommonModule,
    BripleyProductsRoutingModule,
    MatProgressBarModule,
    MatButtonModule
  ]
})
export class BripleyProductsModule { }
