import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/bripley-products-commons/model/product.model';

@Component({
  selector: 'bripley-products-item',
  templateUrl: './products-item.component.html',
  styleUrls: ['./products-item.component.scss']
})
export class ProductsItemComponent implements OnInit {

  @Input()
  product: Product;

  constructor() { }

  ngOnInit(): void {
  }

}
