import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/bripley-products-commons/model/product.model';

@Component({
  selector: 'bripley-products-catalog',
  templateUrl: './products-catalog.component.html',
  styleUrls: ['./products-catalog.component.scss']
})
export class ProductsCatalogComponent implements OnInit {

  @Input()
  products: Product[];

  constructor() { }

  ngOnInit(): void {
  }

}
