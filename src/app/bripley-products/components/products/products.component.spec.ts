import { Component, DebugElement, Input } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BehaviorSubject, Subject } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Product } from 'src/app/bripley-products-commons/model/product.model';
import { ProductService } from 'src/app/bripley-products-commons/services/product.service';
import { ProductsComponent } from './products.component';


@Component({
  selector: 'bripley-products-catalog',
  template: 'mock product catalog componet'
})
export class ProductCatalogMockComponent {

  @Input()
  products: Product[];

}

@Component({
  selector: 'bripley-products-header',
  template: 'mock product header componet'
})
export class ProductHeaderMockComponent {

  @Input()
  size: number;

}

@Component({
  selector: 'mat-progress-bar',
  template: 'mock progress bar'
})
export class MatProgressBarMockComponent {

}

describe('ProductsComponent', () => {

  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;

  let productServiceSpy: jasmine.SpyObj<ProductService>;

  let products: Subject<Product[]>;

  beforeEach(async(() => {

    const spy = jasmine.createSpyObj('ProductService', ['findAll']);

    TestBed.configureTestingModule({
      declarations: [ProductsComponent, ProductCatalogMockComponent, ProductHeaderMockComponent, MatProgressBarMockComponent],
      providers: [{
        provide: ProductService,
        useValue: spy
      }]
    })
      .compileComponents();

    productServiceSpy = TestBed.inject(ProductService) as jasmine.SpyObj<ProductService>;

    products = new BehaviorSubject<Product[]>([]);
    productServiceSpy.findAll.and.returnValue(products);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call product findall', () => {
    expect(productServiceSpy.findAll).toHaveBeenCalled();
  });

  it('product catalog should have products from service', () => {

    const items: Product[] = [
      { id: 'id', name: 'product' }
    ];

    products.next(items);

    const productCatalogComponent: ProductCatalogMockComponent = fixture.debugElement.query(By.directive(ProductCatalogMockComponent)).context;

    fixture.detectChanges();

    expect(productCatalogComponent.products).toEqual(items);

  });

  it('product header should have products size from service', () => {

    const items: Product[] = [
      { id: 'id', name: 'product' }
    ];

    products.next(items);

    const productHeaderComponent: ProductHeaderMockComponent = fixture.debugElement.query(By.directive(ProductHeaderMockComponent)).context;

    fixture.detectChanges();

    expect(productHeaderComponent.size).toEqual(items.length);

  });

  it('should not show progress bar while not loading', () => {

    const matProgressBarComponent: DebugElement = fixture.debugElement.query(By.directive(MatProgressBarMockComponent));

    expect(matProgressBarComponent).toBeFalsy();
  });

  it('should show progress bar while loading', fakeAsync(() => {

    productServiceSpy.findAll.and.returnValue(products.pipe(delay(1)));

    component.ngOnInit();

    fixture.detectChanges();

    const matProgressBarComponent: DebugElement = fixture.debugElement.query(By.directive(MatProgressBarMockComponent));

    expect(matProgressBarComponent).toBeTruthy();

    tick(1);

  }));


});
