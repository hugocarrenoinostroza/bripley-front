import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bripley-products-header',
  templateUrl: './products-header.component.html',
  styleUrls: ['./products-header.component.scss']
})
export class ProductsHeaderComponent implements OnInit {

  @Input()
  size: number;

  constructor() { }

  ngOnInit(): void {
  }

}
