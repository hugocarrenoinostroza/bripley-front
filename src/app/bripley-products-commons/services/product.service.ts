import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  path = '/products';

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(`${environment.apiUrl}${this.path}`);
  }

  findById(id: string): Observable<Product> {
    return this.httpClient.get<Product>(`${environment.apiUrl}${this.path}/${id}`);
  }


}
