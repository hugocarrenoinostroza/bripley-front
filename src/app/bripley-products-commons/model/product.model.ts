export interface Product {
  id: string;
  name: string;
  price?: number;
  description?: string;
  img?: string;
  shortDescription?: string;
  partNumber?: string;
  prices?: any;
  fullImage?: string;
}
