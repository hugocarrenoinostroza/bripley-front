import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppComponent } from './app.component';
import { AuthenticationFacade, AUTHENTICATION_FACADE } from './bripley-authentication/services/authentication-facade/authentication.facade';


@Component({
  selector: 'mat-progress-bar',
  template: 'mock progress bar'
})
export class MatProgressBarMockComponent {

}

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let authenticationFacadeSpy: jasmine.SpyObj<AuthenticationFacade>;

  let checkingAuthentication: Subject<boolean>;
  let isAuthenticated: Subject<boolean>;

  beforeEach(async(() => {

    const spy = jasmine.createSpyObj('AuthenticationFacade', [
      'checkAuthentication', 'checkingAuthentication', 'isAuthenticated', 'logout']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule

      ],
      declarations: [
        AppComponent,
        MatProgressBarMockComponent
      ],
      providers: [
        {
          provide: AUTHENTICATION_FACADE,
          useValue: spy
        }
      ],
      schemas: [NO_ERRORS_SCHEMA

      ]
    }).compileComponents();

    authenticationFacadeSpy = TestBed.inject(AUTHENTICATION_FACADE) as jasmine.SpyObj<AuthenticationFacade>;

    checkingAuthentication = new BehaviorSubject<boolean>(false);
    authenticationFacadeSpy.checkingAuthentication.and.returnValue(checkingAuthentication);


    isAuthenticated = new BehaviorSubject<boolean>(false);
    authenticationFacadeSpy.isAuthenticated.and.returnValue(isAuthenticated);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call checkAuthentication, checkingAuthentication, isAuthenticated', () => {

    expect(authenticationFacadeSpy.checkAuthentication).toHaveBeenCalled();
    expect(authenticationFacadeSpy.checkingAuthentication).toHaveBeenCalled();
    expect(authenticationFacadeSpy.isAuthenticated).toHaveBeenCalled();

  });

  it('should call authentication logout when logout', () => {

    component.logout();

    expect(authenticationFacadeSpy.logout).toHaveBeenCalled();

  });

  it('should show mat progress bar while checking authentication', () => {

    checkingAuthentication.next(true);

    fixture.detectChanges();

    const loadingBar = fixture.debugElement.query(By.directive(MatProgressBarMockComponent));

    expect(loadingBar).toBeTruthy();

  });

  it('should show app', () => {

    checkingAuthentication.next(false);

    fixture.detectChanges();

    const routerOutlet = fixture.debugElement.query(By.css('router-outlet'));

    expect(routerOutlet).toBeTruthy();

  });

  it('should show logout button if is authenticated', () => {

    isAuthenticated.next(true);

    fixture.detectChanges();

    const logoutButton = fixture.debugElement.query(By.css('#logout-button'));

    expect(logoutButton).toBeTruthy();

  });

  it('click logout should call logout from component', () => {

    isAuthenticated.next(true);

    fixture.detectChanges();

    const logoutButton = fixture.debugElement.query(By.css('#logout-button'));

    const spyLogout = spyOn(component, 'logout');

    logoutButton.triggerEventHandler('click', {});

    expect(spyLogout).toHaveBeenCalled();

  });

});
