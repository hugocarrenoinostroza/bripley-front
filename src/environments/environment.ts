// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {

  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyCKxf-pFyrRC248LODRmOYrkAOF09B7UB4',
    authDomain: 'bripley-a2bc5.firebaseapp.com',
    databaseURL: 'https://bripley-a2bc5.firebaseio.com',
    projectId: 'bripley-a2bc5',
    storageBucket: 'bripley-a2bc5.appspot.com',
    messagingSenderId: '603025823277',
    appId: '1:603025823277:web:75553e95648510994e4aa4'
  },

  apiUrl: 'http://localhost:3000'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
